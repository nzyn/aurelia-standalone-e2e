import { browser, element, by, By, $, $$, ExpectedConditions } from 'aurelia-protractor-plugin/protractor';
import { config } from '../protractor.conf';
import { PageObject_Welcome } from './welcome.po';

describe("test app", function () {
  let poWelcome: PageObject_Welcome;

  beforeEach(async () => {
    poWelcome = new PageObject_Welcome();
    await browser.loadAndWaitForAureliaPage(`http://localhost:${config.port}`);
  });

  it("should display welcome message", async () => {
    await expect(await poWelcome.getWelcomeMessage()).toBe("Welcome");
  });
});