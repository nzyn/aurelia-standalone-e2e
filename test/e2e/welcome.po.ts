import { element, by } from "protractor";

export class PageObject_Welcome {
  async getWelcomeMessage(): Promise<string> {
    return await element(by.id("welcome")).getAttribute("innerHTML");
  }
}